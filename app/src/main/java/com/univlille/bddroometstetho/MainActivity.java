package com.univlille.bddroometstetho;

import androidx.annotation.MainThread;
import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.facebook.stetho.Stetho;

import java.util.HashSet;
import java.util.Queue;

import io.reactivex.CompletableObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    private static BookDatabase bookDatabase;
    private CompositeDisposable disposable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Stetho.initializeWithDefaults(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getBookDatabase();
        for (int i = 1; i < 10; i++) {
            BookEntity myBookEntity = new BookEntity();
            myBookEntity.setTitle("Mon titre " + i);
//        bookDatabase.bookDao().addBook(myBookEntity);
            addBook(myBookEntity);
        }
    }

    // comme on ne peut pas charger le thread UI, on utilise un flux React pour l'ajout
    private void addBook(final BookEntity myBookEntity) {
        bookDatabase.bookDao().addBook(myBookEntity)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableCompletableObserver() {
                    @Override
                    public void onComplete() {
                        //Toast.makeText(getApplicationContext(), "OK " + myBookEntity.getId(), Toast.LENGTH_SHORT).show();
                        Log.d("JC", "insertion de " + myBookEntity.getTitle());
                    }

                    @Override
                    public void onError(Throwable e) {
                        //Toast.makeText(getApplicationContext(), "PB !" + e.getMessage(), Toast.LENGTH_SHORT).show();
                        Log.d("JC", e.getMessage());
                    }
                });
    }


    public BookDatabase getBookDatabase() {
        if (bookDatabase == null) {
            bookDatabase = Room.databaseBuilder(getApplicationContext(),
                    BookDatabase.class, "book-database").build();
        }
        return bookDatabase;
    }
}
